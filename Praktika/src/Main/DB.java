package Main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class DB 
{
	public static Connection con;
    public static Statement st;
    public static PreparedStatement pst;
    public static ResultSet resSet;

    public DB()
    {
        getDBConnection();
    }
    
    public static void getDBConnection() 
    {
        con = null;
        String URL = "jdbc:mysql://localhost:3306/praktika";
        String user = "root";
        String password = "Grizzly";



        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("������� ���������");
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            con = DriverManager.getConnection(URL,user,password);
            System.out.println("���������� �����������");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("��� ����������");
        }
    }
    
    public void deletePeople(int id) throws SQLException //TODO Delete
    {
    	try
        {
            st = con.createStatement();
            st.executeUpdate("delete from people where id = "+id+";");
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());}
    }
    
    public void addPeople(People p) throws SQLException //TODO Add
    {
    	 try
         {
             pst = con.prepareStatement(
                     "insert into people"
                             + "(id,name,surname,password)"
                             + "values (?,?,?,?)"
             );

             pst.setInt(1,p.getId());
             pst.setString(2, p.getName());
             pst.setString(3, p.getSurame());
             pst.setString(4, p.getPassword());
             pst.execute();
         }
         catch (Exception e) {
             System.out.println(e.getMessage());}
    }
    
    public ArrayList<People> selectPeople() throws SQLException //TODO Query
    {
    	ArrayList<People> res = new ArrayList<People>();
        try
        {
            st = con.createStatement();
            resSet = st.executeQuery("select * from people;");
            while(resSet.next())
            {
                res.add(new People(resSet));
            }
        } finally
        {
            if(resSet != null)
            {
                resSet.close();
            }
            if (st !=null)
            {
                st.close();
            }
        }
        return res;
    }
    
    public static void CloseDB() throws ClassNotFoundException, SQLException
    {
        con.close();
        st.close();
        resSet.close();
        pst.close();

        System.out.println("���������� �������");
    }

}


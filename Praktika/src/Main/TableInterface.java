package Main;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class TableInterface extends JFrame
{
	String[] columnNames = {"id", "Name", "Surname", "Password"};
	JTextField id, name, surname, password;
	JButton add, delete;
	JPanel inputPanel;
	JTable table;
	JScrollPane scroll;
	DB db;
	
	public TableInterface(final DB db)
	{
		super("�������");
		
		this.db = db;
		
		setBounds(50, 50, 700, 700);
		inputPanel = new JPanel();
        inputPanel.setBackground(Color.LIGHT_GRAY);
        inputPanel.setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(inputPanel);
        
        table = new JTable(new DefaultTableModel(columnNames, 0));
        table.setBounds(0, 0, 400, 400);
        table.setEnabled(false);
        
        scroll = new JScrollPane(table);
        scroll.setBounds(10, 10, 400, 400);
        inputPanel.add(scroll);
        refreshTable();
        
        add = new JButton("��������");
	    add.setBounds(450, 10, 100, 30);
	    add.addActionListener(new ActionListener()
        {
	       	 @Override
	       	    public void actionPerformed(ActionEvent e)
	       	    {
	       		 if(!(id.getText().isEmpty() || name.getText().isEmpty() || surname.getText().isEmpty() || password.getText().isEmpty()))
	       		 	try {
						db.addPeople(
								new People(Integer.parseInt(id.getText()),
										name.getText(),surname.getText(),password.getText()));
						id.setText("");
						name.setText("");
						surname.setText("");
						password.setText("");
	       		 	} catch (SQLException e1) { e1.printStackTrace(); }
	       		 	refreshTable();
	       	    }
        });
	    inputPanel.add(add);
	    
	    delete = new JButton("�������");
	    delete.setBounds(450, 50, 100, 30);
	    delete.addActionListener(new ActionListener()
        {
	       	 @Override
	       	    public void actionPerformed(ActionEvent e)
	       	    {
	       		 if(!id.getText().isEmpty())
	       		 	try {
						db.deletePeople(Integer.parseInt(id.getText()));
					} catch (SQLException e1) { e1.printStackTrace(); }
	       		 	refreshTable();
	       	    }
        });
	    inputPanel.add(delete);
	    
	    id = new JTextField(10);
        id.setBounds(10, 450, 80, 30);
        id.setVisible(true);
        inputPanel.add(id);
        
        name = new JTextField(10);
        name.setBounds(100, 450, 120, 30);
        name.setVisible(true);
        inputPanel.add(name);
        
        surname = new JTextField(10);
        surname.setBounds(230, 450, 120, 30);
        surname.setVisible(true);
        inputPanel.add(surname);
        
        password = new JTextField(10);
        password.setBounds(360, 450, 120, 30);
        password.setVisible(true);
        inputPanel.add(password);
	}
	
	private void refreshTable()
	{
		try { Data.setArrayPeople(db.selectPeople()); } catch (SQLException e) { e.printStackTrace(); }
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--)
            model.removeRow(i);
        for(People p: Data.getArrayPeople())
        	model.addRow(p.toStringArray());
	}
	
	public static void createForm(DB db)
    {
        TableInterface form = new TableInterface(db);
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        form.setVisible(true);
        form.setResizable(false);
    }
}

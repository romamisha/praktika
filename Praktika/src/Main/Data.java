package Main; 

import java.util.ArrayList;


public class Data 
{
	static ArrayList<People> people;
	
	public static void setArrayPeople(ArrayList<People> p)
	{
		people = p;
	}
	
	public static ArrayList<People> getArrayPeople()
	{
		return people;
	}
	
	public static People getPeople(int i)
	{
		return people.get(i);
	}
	
	public static void setPeople(People p)
	{
		people.add(p);
	}
}

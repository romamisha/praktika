package Main;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class LoginInterface extends JFrame
{
	//ArrayList<People> people;
	
	JLabel loginLabel;
	JLabel passwordLabel;
	JPanel inputPanel;
	JTextField login;
	JPasswordField password;
	
	JButton confirm;
	DB db;
	
	public LoginInterface(final DB db)
	{
		super("���� � �������");
		
		this.db = db;
		try { Data.setArrayPeople(db.selectPeople()); } catch (SQLException e) { e.printStackTrace(); }
		
		setBounds(150, 150, 500, 500);
		inputPanel = new JPanel();
        inputPanel.setBackground(Color.LIGHT_GRAY);
        inputPanel.setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(inputPanel);

        //////////////////////////////////////////////////
        
		login = new JTextField(10);
        login.setBounds(150, 100, 300, 30);
        login.setVisible(true);
        login.setToolTipText("������� �����");
        inputPanel.add(login);
        
		password = new JPasswordField();
		password.setBounds(150, 150, 300, 30);
		password.setToolTipText("������� ������");
		password.setVisible(true);
		inputPanel.add(password);
		
		loginLabel = new JLabel("�����:");
		loginLabel.setBounds(70, 100, 300, 30);
        loginLabel.setVisible(true);
        inputPanel.add(loginLabel);
        
        passwordLabel = new JLabel("������:");
		passwordLabel.setBounds(70, 150, 300, 30);
        passwordLabel.setVisible(true);
        inputPanel.add(passwordLabel);
		
		///////////////////////////////////////////////////
		
		confirm = new JButton();
	    confirm.setBounds(200, 300, 100, 30);
	    confirm.setText("�����");
	    confirm.addActionListener(new ActionListener()
        {
	       	 @Override
	       	    public void actionPerformed(ActionEvent e)
	       	    {
	       		 	String id = login.getText();
	       		 	String pw = password.getText();

	       		 	for(People p: Data.getArrayPeople())
	       		 	{
	       		 		if(id.equals(Integer.toString(p.getId())) && pw.equals(p.getPassword()))
	       		 		{
	       		 			TableInterface.createForm(db);
	       		 			setVisible(false);
	       		 			break;
	       		 		}
	       		 	}
	       	    }
        });
	    inputPanel.add(confirm);
	}
	
	public static void createForm(DB db)
    {
        LoginInterface form = new LoginInterface(db);
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        form.setVisible(true);
        form.setResizable(false);
    }
}

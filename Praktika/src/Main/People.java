package Main;


import java.sql.ResultSet;
import java.sql.SQLException;

public class People 
{
	private int id;
	private String name;
	private String surname;
	private String password;
	
	public People(ResultSet rs) throws SQLException
    {
    	id = rs.getInt("id");
    	name = rs.getString("name");
    	surname = rs.getString("surname");
    	password = rs.getString("password");
    }
    
    public People(int id, String name, String surname, String password)
    {
    	this.id = id;
    	this.name = name;
    	this.surname = surname;
    	this.password = password;
    }

    public void setId(int v) { id = v; }
    public int getId(){ return id; }
    
    public void setName(String v) { name = v; }
    public String getName(){ return name; }
    
    public void setSurame(String v) { surname = v; }
    public String getSurame(){ return surname; }

    public void setPassword(String v) { password = v; }
    public String getPassword(){ return password; }

    @Override public String toString()
    {
        return "| " + id + " | " + name + " | "+ surname + " | ";
    }
    
    public String[] toStringArray()
    {
    	return new String[]{ Integer.toString(id), name, surname, password};
    }
}
